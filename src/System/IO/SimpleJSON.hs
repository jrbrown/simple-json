module System.IO.SimpleJSON
    ( writeJson
    , writeIOJson
    , writeHaskToJson
    , writeIOHaskToJson
    , readJson
    , readJsonAll
    , readJsonToHask
    , readJsonAllToHask
    ) where


import Data.SimpleJSON
import Data.SimpleJSON.Encoding (encodeJson)
import Data.SimpleJSON.Print (prettyPrint)
import Data.SimpleJSON.Parser (jsonParse, jsonParseAllValid)
import Data.Maybe (listToMaybe)


writeJson :: FilePath -> JsonVal -> IO ()
writeJson fp = writeFile fp . prettyPrint . encodeJson

writeIOJson :: FilePath -> IO JsonVal -> IO ()
writeIOJson fp ioVal = ioVal >>= writeJson fp

writeHaskToJson :: (JsonConvertable a) => FilePath -> a -> IO ()
writeHaskToJson fp = writeJson fp . toJsonVal

writeIOHaskToJson :: (JsonConvertable a) => FilePath -> IO a -> IO ()
writeIOHaskToJson fp ioVal = ioVal >>= writeHaskToJson fp

readJsonAll :: FilePath -> IO [JsonVal]
readJsonAll fp = jsonParseAllValid <$> readFile fp

readJson :: FilePath -> IO (Maybe JsonVal)
readJson fp = jsonParse <$> readFile fp

readJsonAllToHask :: (JsonConvertable a) => FilePath -> IO [a]
readJsonAllToHask fp = fromJsonVals <$> readJsonAll fp

readJsonToHask :: (JsonConvertable a) => FilePath -> IO (Maybe a)
readJsonToHask fp = listToMaybe <$> readJsonAllToHask fp

