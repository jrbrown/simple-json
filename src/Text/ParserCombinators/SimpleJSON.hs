module Text.ParserCombinators.SimpleJSON where


import Data.Char (toUpper, toLower)
import Data.Maybe (listToMaybe)

import Text.ParserCombinators.ReadP


-- TODO: upgrade this to use Control.Monad.Except
singleOnlyFullParse :: ReadP a -> String -> Either String a
singleOnlyFullParse prs str = case fullParseOnly prs str of
                                []  -> Left "No successful full parses"
                                [x] -> Right x
                                _   -> Left "Multiple parses"

atLeastOneFullParse :: ReadP a -> String -> Maybe a
atLeastOneFullParse prs = listToMaybe . fullParseOnly prs

fullParseOnly :: ReadP a -> String -> [a]
fullParseOnly prs str = [x | (x, l) <- readP_to_S prs str, null l]

containsParse :: ReadP a -> String -> [a]
containsParse prs str = fst <$> readP_to_S (skipTill prs) str


skipTill :: ReadP a -> ReadP a
skipTill end = end <++ (get >> skipTill end)


anyLeft :: ReadP a -> ReadP Bool
anyLeft p = do
    txt_left <- look
    if null $ containsParse p txt_left
       then return False
       else return True


getLast :: ReadP a -> ReadP a
getLast p = do
    next <- skipTill p
    any_left <- anyLeft p
    if any_left
       then getLast p
       else return next


capsVars :: String -> [String]
capsVars "" = [""]
capsVars str = [lcase, caps, ucase]
    where lcase = map toLower str
          caps = head ucase : tail lcase
          ucase = map toUpper str

oneOf :: [Char] -> ReadP Char
oneOf = satisfy . flip elem

notOneOf :: [Char] -> ReadP Char
notOneOf = satisfy . flip notElem

stringChoice :: [String] -> ReadP String
stringChoice = choice . map string

whiteSpace :: ReadP Char
whiteSpace = oneOf [' ', '\n', '\t', '\r']

whiteSpaces :: ReadP String
whiteSpaces = many whiteSpace

notWhiteSpace :: ReadP Char
notWhiteSpace = notOneOf [' ', '\n', '\t', '\r']

notWhiteSpaces :: ReadP String
notWhiteSpaces = many1 notWhiteSpace

commaSep :: ReadP String
commaSep = do
    sep <- char ','
    ws <- whiteSpaces
    return (sep:ws)

colonSep :: ReadP String
colonSep = do
    sep <- char ':'
    ws <- whiteSpaces
    return (sep:ws)

digit :: ReadP Char
digit = oneOf ['0'..'9']

digits :: ReadP String
digits = many1 digit

signedDigits :: ReadP String
signedDigits = do
    s <- choice [char '+', char '-']
    d <- digits
    return (s:d)

intStr :: ReadP String
intStr = choice [digits, signedDigits]

int :: ReadP Int
int = read <$> intStr

integer :: ReadP Integer
integer = read <$> intStr

floatRHSa :: ReadP String
floatRHSa = do
    m <- choice [char '.', char 'e', char 'E']
    r <- intStr
    return (m:r)

floatRHSb :: ReadP String
floatRHSb = do
    char '.'
    d1 <- digits
    e <- choice [char 'e', char 'E']
    d2 <- intStr
    return ('.':d1++[e]++d2)

floatRHSc :: ReadP String
floatRHSc = do
    char '.'
    return []

floatStr :: ReadP String
floatStr = do
    l <- intStr
    r <- choice [floatRHSa, floatRHSb, floatRHSc]
    return (l++r)

float :: ReadP Double
float = read <$> floatStr

singleQuoteStr :: ReadP String
singleQuoteStr = do
    char '\''
    str <- many $ satisfy (/= '\'')
    char '\''
    return str

doubleQuoteStr :: ReadP String
doubleQuoteStr = do
    char '"'
    str <- many $ satisfy (/= '"')
    char '"'
    return str

anyString :: ReadP String
anyString = singleQuoteStr +++ doubleQuoteStr

boolStr :: ReadP String
boolStr = stringChoice (capsVars "True" ++ capsVars "False")

bool :: ReadP Bool
bool = (`elem` capsVars "True") <$> boolStr

nullStr :: ReadP String
nullStr =  (stringChoice . capsVars) "null"

