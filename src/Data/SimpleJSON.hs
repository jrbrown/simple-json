{-# LANGUAGE FlexibleInstances #-}

module Data.SimpleJSON
    ( JsonVal (..)
    , JsonConvertable
    , toJsonVal
    , fromJsonVal
    , fromJsonVals
    , realToJsonVal
    , jsonValToFrac
    ) where

import Data.Map (Map)
import Data.Maybe (mapMaybe)


data JsonVal = JObj (Map String JsonVal)
             | JArray [JsonVal]
             | JINum Integer
             | JENum Double
             | JString String
             | JBool Bool 
             | JNull
             deriving (Eq, Show, Ord, Read)


class JsonConvertable a where
    toJsonVal :: a -> JsonVal
    fromJsonVal :: JsonVal -> Maybe a


fromJsonVals :: (JsonConvertable a) => [JsonVal] -> [a]
fromJsonVals = mapMaybe fromJsonVal

realToJsonVal :: (Real a) => a -> JsonVal
realToJsonVal num = JENum (realToFrac num)

jsonValToFrac :: (Fractional a) => JsonVal -> Maybe a
jsonValToFrac (JENum num) = Just (realToFrac num)
jsonValToFrac (JINum num) = Just (fromInteger num)
jsonValToFrac _ = Nothing


instance JsonConvertable JsonVal where
    toJsonVal = id
    fromJsonVal = Just

instance (JsonConvertable a) => JsonConvertable (Map String a) where
    toJsonVal obj = JObj (fmap toJsonVal obj)
    fromJsonVal (JObj obj) = mapM fromJsonVal obj
    fromJsonVal _ = Nothing

instance {-# OVERLAPPABLE #-} (JsonConvertable a) => JsonConvertable [a] where
    toJsonVal lst = JArray (fmap toJsonVal lst)
    fromJsonVal (JArray lst) = mapM fromJsonVal lst
    fromJsonVal _ = Nothing

instance JsonConvertable Integer where
    toJsonVal = JINum
    fromJsonVal (JINum num) = Just num
    fromJsonVal _ = Nothing

instance JsonConvertable Int where
    toJsonVal num = JINum (toInteger num)
    fromJsonVal (JINum num) = Just (fromInteger num)
    fromJsonVal _ = Nothing

instance JsonConvertable Double where
    toJsonVal = JENum
    fromJsonVal (JENum num) = Just num
    fromJsonVal (JINum num) = Just (fromInteger num)
    fromJsonVal _ = Nothing

instance JsonConvertable Float where
    toJsonVal = realToJsonVal
    fromJsonVal = jsonValToFrac

instance JsonConvertable String where
    toJsonVal = JString
    fromJsonVal (JString str) = Just str
    fromJsonVal _ = Nothing

instance JsonConvertable Bool where
    toJsonVal = JBool
    fromJsonVal (JBool bool) = Just bool
    fromJsonVal _ = Nothing

instance (JsonConvertable a) => JsonConvertable (Maybe a) where
    toJsonVal (Just x) = toJsonVal x
    toJsonVal Nothing = JNull
    fromJsonVal JNull = Just Nothing
    fromJsonVal x = Just (fromJsonVal x)

