module Data.SimpleJSON.Parser
    ( unsafeJsonParse
    , jsonParse
    , jsonParseAllValid
    , jVal
    ) where


import Data.SimpleJSON (JsonVal (..))
import Data.Map (fromList)
import Data.Maybe (listToMaybe, fromMaybe)

import Text.ParserCombinators.ReadP
import Text.ParserCombinators.SimpleJSON


unsafeJsonParse :: String -> JsonVal
unsafeJsonParse = fromMaybe (error "No valid json parse") . jsonParse

jsonParse :: String -> Maybe JsonVal
jsonParse = listToMaybe . jsonParseAllValid

jsonParseAllValid :: String -> [JsonVal]
jsonParseAllValid = fullParseOnly jValWhiteSpace

jValWhiteSpace :: ReadP JsonVal
jValWhiteSpace = do
    jval <- jVal
    whiteSpaces
    return jval

jVal :: ReadP JsonVal
jVal = choice [jObj, jArray, jINum, jENum, jBool, jNull, jString]

jDictElem :: ReadP (String, JsonVal)
jDictElem = do
    str <- anyString
    colonSep
    val <- jVal
    return (str, val)

jObj :: ReadP JsonVal
jObj = do
    char '{'
    whiteSpaces
    init_vals <- sepBy jDictElem commaSep
    whiteSpaces
    char '}'
    return (JObj (fromList init_vals))

jArray :: ReadP JsonVal
jArray = do
    char '['
    whiteSpaces
    vals <- sepBy jVal commaSep
    whiteSpaces
    char ']'
    return (JArray vals)

jINum :: ReadP JsonVal
jINum = JINum <$> integer

jENum :: ReadP JsonVal
jENum = JENum <$> float

jString :: ReadP JsonVal
jString = JString <$> anyString

jBool :: ReadP JsonVal
jBool = JBool <$> bool

jNull :: ReadP JsonVal
jNull = JNull <$ nullStr

