module Data.SimpleJSON.Print
    ( prettyPrint
    , prettyPrintParser
    ) where


import Control.Applicative ((<|>))
import Data.Maybe (fromMaybe)

import Text.ParserCombinators.ReadP
import Text.ParserCombinators.SimpleJSON


type IndentLevel = Int


indent :: String
indent = "    "

prettyPrint :: String -> String
prettyPrint str = fromMaybe str (atLeastOneFullParse prettyPrintParser str)

prettyPrintParser :: ReadP String
prettyPrintParser = val <|> structure 0

val :: ReadP String
val = choice [intStr, floatStr, quotedString, boolStr, nullStr]

quotedString :: ReadP String
quotedString = do
    str <- anyString
    return ("\"" ++ str ++ "\"")

structure :: IndentLevel -> ReadP String
structure ind = choice (map (\x -> x ind) [list, dict])

getBody :: String -> [String] -> String
getBody ind1Str vals
  | null vals = ""
  | otherwise = bodyInitStr ++ bodyLastStr
    where
        bodyInitStr = concatMap (\x -> ('\n' : ind1Str) ++ x ++ ",") (init vals)
        bodyLastStr = ('\n' : ind1Str) ++ last vals

genericStructure :: Char -> Char -> (IndentLevel -> ReadP [String]) -> IndentLevel -> ReadP String
genericStructure start_token end_token elems_reader ind = do
    let ind1 = ind + 1
    let indStr = concat $ replicate ind indent
    let ind1Str = concat $ replicate ind1 indent

    char start_token
    whiteSpaces
    vals <- elems_reader ind1
    whiteSpaces
    char end_token

    let body = getBody ind1Str vals

    return ([start_token] ++ body ++ "\n" ++ indStr ++ [end_token])

list :: IndentLevel -> ReadP String
list = genericStructure '[' ']' elems_reader
    where elems_reader ind = sepBy (val <|> structure ind) commaSep

dictElem :: IndentLevel -> ReadP String
dictElem ind = do
    key <- quotedString
    colonSep
    x <- val <|> structure ind
    return (key ++ ": " ++ x)

dict :: IndentLevel -> ReadP String
dict = genericStructure '{' '}' elems_reader
    where elems_reader ind = sepBy (dictElem ind) commaSep

