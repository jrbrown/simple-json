module Data.SimpleJSON.Encoding
    ( encodeJson
    , prettyEncodeJson
    ) where

import Data.SimpleJSON (JsonVal (..))
import Data.SimpleJSON.Print (prettyPrint)

import Data.List (intercalate)
import Data.Map (toList)


-- TODO: Using bytestrings either by default or as an option

encodeJson :: JsonVal -> String
encodeJson (JObj val) = "{" ++ intercalate "," entryPairs ++ "}"
    where entryPairs = map (\(s,o) -> show s ++ ":" ++ encodeJson o) (toList val)
encodeJson (JArray val) = "[" ++ intercalate "," entries ++ "]"
    where entries = map encodeJson val
encodeJson (JINum val) = show val
encodeJson (JENum val) = show val
encodeJson (JString val) = show val
encodeJson (JBool val) = if val then "true" else "false"
encodeJson JNull = "null"

prettyEncodeJson :: JsonVal -> String
prettyEncodeJson = prettyPrint . encodeJson

