module Data.SimpleJSON.Transforms
    ( mkHaskFunc
    , mkJsonFunc
    , autoParseFunc
    , autoPrettyParseFunc
    , autoParseHaskFunc
    , autoPrettyParseHaskFunc
    ) where


import Data.SimpleJSON (JsonConvertable, JsonVal, toJsonVal, fromJsonVal)
import Data.SimpleJSON.Parser (unsafeJsonParse)
import Data.SimpleJSON.Encoding (encodeJson)
import Data.SimpleJSON.Print (prettyPrint)


maybeFail :: String -> Maybe a -> a
maybeFail _ (Just x) = x
maybeFail str Nothing  = error str


mkHaskFunc :: (JsonConvertable a, JsonConvertable b) => (JsonVal -> JsonVal) -> (a -> b)
mkHaskFunc jFunc = maybeFail "JsonVal could not be converted into the specified data type" . fromJsonVal . jFunc . toJsonVal

mkJsonFunc :: (JsonConvertable a, JsonConvertable b) => (a -> b) -> (JsonVal -> JsonVal)
mkJsonFunc hFunc =  toJsonVal . hFunc . maybeFail "Data could not be converted to JsonVal" . fromJsonVal

autoParseFunc :: (JsonVal -> JsonVal) -> (String -> String)
autoParseFunc jFunc = encodeJson . jFunc . unsafeJsonParse

autoPrettyParseFunc :: (JsonVal -> JsonVal) -> (String -> String)
autoPrettyParseFunc jFunc = prettyPrint . autoParseFunc jFunc

autoParseHaskFunc :: (JsonConvertable a, JsonConvertable b) => (a -> b) -> (String -> String)
autoParseHaskFunc = autoParseFunc . mkJsonFunc

autoPrettyParseHaskFunc :: (JsonConvertable a, JsonConvertable b) => (a -> b) -> (String -> String)
autoPrettyParseHaskFunc = autoPrettyParseFunc . mkJsonFunc

